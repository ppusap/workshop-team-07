#Workshop_team07

##Repository Link
Link to our repository
 - [Repo Link](https://bitbucket.org/ppusap/workshop-team-07/src/master/)

##Team Slide
 ![vscodeimage](https://bitbucket.org/ppusap/workshop-team-07/raw/master/Team.jpeg)
 

##References
  - [Node.js-Express](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/routes)

##Some useful tutorials for beginners
 - [MVC for beginners](https://youtu.be/QseHOX-5nJQ)
 - [Tutorial Node.js-Express](https://youtu.be/GLL3UIPgU_o)

